package com.almundo.trains.service.utils;

import com.almundo.trains.dto.model.PriceDto;
import com.almundo.trains.model.Price;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;

@Component
public class PriceService {
    public PriceDto trainAvailabilityPrice(Price price, Integer infant, Integer child, Integer adult) {

        return new PriceDto( price.getCurrency(),  BigDecimal.valueOf(infant).multiply(price.getInfant())
                .add(BigDecimal.valueOf(child).multiply(price.getChild()))
                .add(BigDecimal.valueOf(adult).multiply(price.getAdult())));

    }
}
