package com.almundo.trains.service.utils;

import com.almundo.trains.dto.model.ServiceTypeDto;
import com.almundo.trains.dto.model.StationDto;
import com.almundo.trains.dto.model.TrainDto;
import com.almundo.trains.dto.response.AvailabilityResponse;
import com.almundo.trains.model.Departure;
import com.almundo.trains.model.ServiceType;
import com.almundo.trains.model.Station;
import com.almundo.trains.model.Train;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Component
public class TrainAvailabilityTransformer {

    @Autowired
    private PriceService priceService;

    public AvailabilityResponse toTrainAvailabilityResponse(List<Train> trains, LocalDate departureDate, Integer infant, Integer child, Integer adult) {
        //TODO: para cada uno de los horarios de partida tengo que
        //armar una disponibilidad para que quede listado por servicio
        AvailabilityResponse availabilityResponse = new AvailabilityResponse();
        List<TrainDto> trainOptions = new ArrayList<>();

        if( !CollectionUtils.isEmpty(trains) ) {
            //TODO: pasar esto aggregate de mongo
            trains.forEach(train -> {
                List<Departure> departures = train.getDepartureList();
                for (Departure departure : departures) {
                    List<String> departureTimes = departure.getDepartureTimes();

                    for (String departureTime : departureTimes) {
                        trainOptions.add(toTrain(train, departureDate, departureTime, infant, child, adult));
                    }
                }
            });
        }
        availabilityResponse.setTrainOptions(trainOptions);
        return availabilityResponse;
    }

    private TrainDto toTrain(Train train, LocalDate departureDate, String departureTime, Integer infant, Integer child, Integer adult) {
        TrainDto trainDto = new TrainDto();
        trainDto.setId(train.getId());
        trainDto.setOrigin(toStationDto(train.getOrigin()));
        trainDto.setDestination(toStationDto(train.getDestination()));
        trainDto.setServiceType(toServiceTypeDto(train.getServiceType()));
        trainDto.setDepartureDate(departureDate.format(DateTimeFormatter.ISO_LOCAL_DATE));
        trainDto.setDepartureTime(departureTime);
        trainDto.setProvider(train.getProvider());
        trainDto.setPrice(priceService.trainAvailabilityPrice(train.getPrice(), infant, child, adult ));

        return trainDto;

    }

    private ServiceTypeDto toServiceTypeDto(ServiceType trainServiceType) {
        if( Objects.nonNull(trainServiceType)) {
            return ServiceTypeDto.valueOf(trainServiceType.name());
        }else{
            return ServiceTypeDto.ECONOMIC;
        }
    }

    private StationDto toStationDto(Station station) {
        StationDto stationDto = new StationDto();
        stationDto.setName(station.getName());
        stationDto.setAdress(station.getAdress());

        return stationDto;
    }

}
