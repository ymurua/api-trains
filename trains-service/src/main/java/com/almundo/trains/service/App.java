package com.almundo.trains.service;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

import java.time.LocalTime;

@SpringBootApplication
@ComponentScan({"com.almundo.api",
"com.almundo.trains.model",
        "com.almundo.trains.repository",
        "com.almundo.trains.service",
})

@EnableMongoRepositories("com.almundo.trains.repository")
public class App {

    private static Class<App> application = App.class;
    public static void main(String[] args) {

        SpringApplication.run(App.class, args);
    }
}