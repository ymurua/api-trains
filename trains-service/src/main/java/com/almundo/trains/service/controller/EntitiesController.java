package com.almundo.trains.service.controller;

import com.almundo.trains.model.City;
import com.almundo.trains.model.Station;
import com.almundo.trains.model.Train;
import com.almundo.trains.repository.exceptions.EntityNotFoundException;
import com.almundo.trains.repository.service.CityService;
import com.almundo.trains.repository.service.StationService;
import com.almundo.trains.repository.service.TrainService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;

@RestController
@RequestMapping("entities")
public class EntitiesController {

    private CityService cityService;
    private StationService stationService;
    private TrainService trainService;

    @Autowired
    protected EntitiesController(CityService cityService, StationService stationService,
            TrainService trainService) {
        this.cityService = cityService;
        this.stationService = stationService;
        this.trainService = trainService;
    }

    @PostMapping("cities")
    public City createCity(
            @RequestParam String code,
            @RequestParam String country,
            @RequestParam String name) {
        return cityService.createCity(code, name, country);
    }

    @GetMapping("cities/{code}")
    public City getCity(@PathVariable String code) throws EntityNotFoundException {
        return cityService.getCity(code);
    }

    @GetMapping("cities")
    public List<City> getAllCities(){
        return cityService.getAllCities();
    }

    @PostMapping("stations")
    public Station createStation(
            @RequestParam String code,
            @RequestParam String name,
            @RequestParam String cityCode,
            @RequestParam String address){
        return stationService.createStation(code, name, cityCode, address);
    }

    //TODO: agregar código de estación y buscar por código de estación y no por nombre
    @GetMapping("stations/{code}")
    public Station getStation(@PathVariable String code){
        return stationService.getStation(code);
    }

    @GetMapping("stations")
    public List<Station> getAllStations(){
        return stationService.getAllStations();
    }

    @PostMapping("trains")
    public Train createTrain(
            @RequestParam String code,
            @RequestParam String origin,
            @RequestParam String destination,
            @RequestParam String provider
            //Todo: agregar pais
    ){
        return trainService.createTrain(code, origin, destination, provider);
    }

    @PutMapping("trains/departures/{code}")
    public Train updateDepartureDates(
            //TODO: ver cómo hacer cuando quiero agregar otro Departure a un
            //TODO: servicio que ya tiene un departure
            @PathVariable String code,
            @RequestParam(name = "startDate") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate startDate,
            @RequestParam(name = "endDate") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate endDate,//@RequestParam LocalDate endDate
           // @RequestParam(name = "departureTimes") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) List<LocalDate> departureTimes//@RequestParam LocalDate endDate
          @RequestParam List<String> departureTimes
    ){
        return trainService.updateDepartureDates(code, startDate, endDate, departureTimes);
    }

    @PutMapping("trains/price/{code}")
    public Train updatePrice(
            @PathVariable String code,
            @RequestParam BigDecimal infant,
            @RequestParam BigDecimal child,
            @RequestParam BigDecimal adult,
            @RequestParam String currency
    ){
        return trainService.updatePrice(code, currency, infant, child, adult);
    }

    @GetMapping("trains")
    public List<Train> getAllTrains(){
        return trainService.getAllTrains();
    }
}
