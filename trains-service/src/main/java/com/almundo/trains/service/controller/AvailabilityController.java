package com.almundo.trains.service.controller;

import com.almundo.trains.dto.response.AvailabilityResponse;
import com.almundo.trains.service.TrainAvailabilityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDate;

@RestController
@RequestMapping("availabilities")
public class AvailabilityController {

    private TrainAvailabilityService trainAvailabilityService;

    @Autowired
    protected AvailabilityController(TrainAvailabilityService trainAvailabilityService) {
        this.trainAvailabilityService = trainAvailabilityService;
    }

    //TODO: o esto debería ser un post y guardar todas las posibles combinaciones? - Por ahora solamente lo muestra, no lo guarda
    @GetMapping("trains")
    public AvailabilityResponse getTrainAvailabilities( @RequestParam String origin,
            @RequestParam String destination,
            @RequestParam(name = "date") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate date,
            @RequestParam Integer infants,
            @RequestParam Integer childs,
            @RequestParam Integer adults){
        return trainAvailabilityService.

                getAvailability(origin, destination, date, infants, childs, adults);
    }

    //TODO: hacer un post para guardar el availability seleccionado- toavía no
    //TODO: para eso necesito que mi cluster tenga un id, para poder identificarlo y cuando sea elegido poder ser guardado.

}
