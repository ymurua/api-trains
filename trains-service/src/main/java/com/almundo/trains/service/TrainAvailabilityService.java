package com.almundo.trains.service;

import com.almundo.trains.dto.response.AvailabilityResponse;
import com.almundo.trains.model.Train;
import com.almundo.trains.repository.service.TrainService;
import com.almundo.trains.service.utils.TrainAvailabilityTransformer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;

@Service
public class TrainAvailabilityService {

    private TrainService trainService;
    private TrainAvailabilityTransformer trainAvailabilityTransformer;

    public TrainAvailabilityService(TrainService trainService,
            TrainAvailabilityTransformer trainAvailabilityTransformer) {
        this.trainService = trainService;
        this.trainAvailabilityTransformer = trainAvailabilityTransformer;
    }

    final static Logger logger = LoggerFactory.getLogger(TrainAvailabilityService.class);

    public AvailabilityResponse getAvailability(String origin, String destination, LocalDate date,
            Integer infant, Integer child, Integer adult){

        logger.info("Consultando disponibilidad");

       if(correctDate(date) && correctPassengers(infant,child,adult)) {
           List<Train> train = trainService.getAvailability(origin, destination, date);

           AvailabilityResponse trainOptions = trainAvailabilityTransformer
                   .toTrainAvailabilityResponse(train, date, infant, child, adult);

           return trainOptions;
       }else{
           return null;
       }
    }

    private boolean correctPassengers(Integer infant, Integer child, Integer adult) {

        Float adultFloat = new Float (adult);
        Float infantFloat = new Float (infant);
        Float childFloat = new Float (child);
        return  adultFloat / (infantFloat + childFloat)> 0.5 && infant+child+adult<10;
    }

    private boolean correctDate(LocalDate date) {
        return date.isAfter(LocalDate.now());
    }
}
