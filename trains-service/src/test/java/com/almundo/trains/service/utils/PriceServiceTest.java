package com.almundo.trains.service.utils;

import com.almundo.trains.dto.model.PriceDto;
import com.almundo.trains.model.Price;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.junit.MockitoJUnitRunner;

import java.math.BigDecimal;

@RunWith(MockitoJUnitRunner.class)

public class PriceServiceTest {

    @InjectMocks
    PriceService priceService;

    @Test
    public void calculateTrainAvailabilityPrice(){

        BigDecimal infantPrice = BigDecimal.valueOf(1.0);
        BigDecimal childPrice = BigDecimal.valueOf(5.0);
        BigDecimal adultPrice = BigDecimal.valueOf(15.0);

        Integer infantCount = 1;
        Integer childCount = 2;
        Integer adultCount = 2;

        Price price = new Price("currency", infantPrice, childPrice, adultPrice);

        PriceDto expected = new PriceDto("currency", BigDecimal.valueOf(41));

        PriceDto result = priceService.trainAvailabilityPrice(price,infantCount, childCount, adultCount);


        Assert.assertEquals(expected, result);
    }

}