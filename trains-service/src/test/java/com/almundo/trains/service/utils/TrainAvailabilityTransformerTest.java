package com.almundo.trains.service.utils;

import com.almundo.trains.dto.model.PriceDto;
import com.almundo.trains.dto.model.ServiceTypeDto;
import com.almundo.trains.dto.model.StationDto;
import com.almundo.trains.dto.model.TrainDto;
import com.almundo.trains.dto.response.AvailabilityResponse;
import com.almundo.trains.model.*;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;

import static org.mockito.Mockito.any;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class TrainAvailabilityTransformerTest {


    @Mock
    PriceService priceService;

    @InjectMocks
    TrainAvailabilityTransformer trainAvailabilityTransformer;


    @Test
    public void toTrainAvailabilityResponse() {

        List<Train> trainList = getTrains();
        LocalDate departureDate =  LocalDate.of(2020,10,1);

        PriceDto priceDto = new PriceDto("currency", BigDecimal.valueOf(3));
        AvailabilityResponse expected = getAvailabilityResponse();

        when(priceService.trainAvailabilityPrice(any(), any(),any(),any())).thenReturn(priceDto);

        AvailabilityResponse result = trainAvailabilityTransformer.toTrainAvailabilityResponse(
                trainList,departureDate,1,1,1);

        Assert.assertEquals(expected, result);
    }


    private List<Train> getTrains(){

        City originCity = new City("originCity_code", "originCity_name", "originCity_county");
        City destinationCity = new City("destinationCity_code", "destinationCity_name", "destinationCity_county");

        Station originStation = new Station("Station01", "OriginStation_name", "OriginStation_adress", originCity);
        Station destinationStation = new Station("Station02", "DestinationStation_name", "DestinationStation_adress", destinationCity);

        Departure departure = new Departure(
                LocalDate.of(2020,1,1),
                LocalDate.of(2021,12,31),
                Arrays.asList("10:10", "12:00"));

        Train train = new Train( "test001",  originStation,  destinationStation,  "TrainProvider");
        train.setDepartureList(Arrays.asList(departure));
        train.setServiceType(ServiceType.ECONOMIC);
        train.setPrice(new Price("USD", BigDecimal.valueOf(1), BigDecimal.valueOf(5), BigDecimal.valueOf(20)) );

        return Arrays.asList(train);
        }

        private AvailabilityResponse getAvailabilityResponse(){

            AvailabilityResponse availabilityResponse = new AvailabilityResponse();

            StationDto originStation = new StationDto();
            originStation.setName("OriginStation_name");
            originStation.setAdress("OriginStation_adress");

            StationDto destinationStation = new StationDto();
            destinationStation.setName("DestinationStation_name");
            destinationStation.setAdress("DestinationStation_adress");

            TrainDto expectedTrain1 = new TrainDto();
            expectedTrain1.setId("test001");
            expectedTrain1.setOrigin(originStation);
            expectedTrain1.setDestination(destinationStation);
            expectedTrain1.setDepartureDate( "2020-10-01");
            expectedTrain1.setDepartureTime("10:10");
            expectedTrain1.setProvider("TrainProvider");
            expectedTrain1.setServiceType(ServiceTypeDto.ECONOMIC);
            expectedTrain1.setPrice(new PriceDto("currency",BigDecimal.valueOf(3)));

            TrainDto expectedTrain2 = new TrainDto();
            expectedTrain2.setId("test001");
            expectedTrain2.setOrigin(originStation);
            expectedTrain2.setDestination(destinationStation);
            expectedTrain2.setDepartureDate( "2020-10-01");
            expectedTrain2.setDepartureTime("12:00");
            expectedTrain2.setProvider("TrainProvider");
            expectedTrain2.setServiceType(ServiceTypeDto.ECONOMIC);
            expectedTrain2.setPrice(new PriceDto("currency",BigDecimal.valueOf(3)));

            availabilityResponse.setTrainOptions(Arrays.asList(expectedTrain1, expectedTrain2));
            return availabilityResponse;
        }

}