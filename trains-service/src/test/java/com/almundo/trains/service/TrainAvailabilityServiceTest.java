package com.almundo.trains.service;

import com.almundo.trains.dto.model.PriceDto;
import com.almundo.trains.dto.model.ServiceTypeDto;
import com.almundo.trains.dto.model.StationDto;
import com.almundo.trains.dto.model.TrainDto;
import com.almundo.trains.dto.response.AvailabilityResponse;
import com.almundo.trains.model.*;
import com.almundo.trains.repository.service.TrainService;
import com.almundo.trains.service.utils.PriceService;
import com.almundo.trains.service.utils.TrainAvailabilityTransformer;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class TrainAvailabilityServiceTest {

    //Verificar:
    /*
    2) al menos 1 pasajero cada 2 menores (child or infant) ok
    3) que la estación exista
    4) que la ciudad exista
    5) pasajeros totales 9
    6) que el servicio exista (para datos válidos)
    7) que el servicio no exista (para datos válidos)
    8)


     */

    @Mock
    TrainService trainService;

    @Mock
    TrainAvailabilityTransformer trainAvailabilityTransformer;

    @Mock
    PriceService priceService;


    @InjectMocks
    TrainAvailabilityService availabilityService;

    @Test
    public void trainAvailability_getAvailability(){

        //Arrange
        Train train1 = Mockito.mock(Train.class);
        Train train2 = Mockito.mock(Train.class);
        List<Train> trainList = Arrays.asList(train1, train2);

        AvailabilityResponse expectedAvailabilityResponse = getAvailabilityResponse();

        when(trainService.getAvailability(any(), any(), any()))
                .thenReturn(trainList);
        when(trainAvailabilityTransformer.toTrainAvailabilityResponse(any(),any(),any(),any(),any())).thenReturn(Mockito.mock(AvailabilityResponse.class));

       availabilityService.getAvailability( "originCity_code",
                "destinationCity_code",LocalDate.now().plusDays(1),
                1,2,3);

       Mockito.verify(trainService, times(1)).getAvailability(any(),any(),any());
       Mockito.verify(trainAvailabilityTransformer, times(1)).toTrainAvailabilityResponse(any(),any(),any(),any(),any());
    }


    //La fecha de salida es al menos un día más que el actual
    @Test
    public void trainAvailability_getAvailabilityForToday(){

        //Arrange
        Train train1 = Mockito.mock(Train.class);
        Train train2 = Mockito.mock(Train.class);
        List<Train> trainList = Arrays.asList(train1, train2);

        AvailabilityResponse expectedAvailabilityResponse = getAvailabilityResponse();

        LocalDate today = LocalDate.now();
        availabilityService.getAvailability( "originCity_code",
                "destinationCity_code", today,
                1,2,3);

        Mockito.verify(trainService, times(0)).getAvailability(any(),any(),any());
        Mockito.verify(trainAvailabilityTransformer, times(0)).toTrainAvailabilityResponse(any(),any(),any(),any(),any());
    }

    @Test
    public void trainAvailability_getAvailabilityForTomorrow(){

        //Arrange
        Train train1 = Mockito.mock(Train.class);
        Train train2 = Mockito.mock(Train.class);
        List<Train> trainList = Arrays.asList(train1, train2);

        AvailabilityResponse expectedAvailabilityResponse = getAvailabilityResponse();

        LocalDate tomorrow = LocalDate.now().plusDays(1L);
        availabilityService.getAvailability( "originCity_code",
                "destinationCity_code", tomorrow,
                1,2,3);

        Mockito.verify(trainService, times(1)).getAvailability(any(),any(),any());
        Mockito.verify(trainAvailabilityTransformer, times(1)).toTrainAvailabilityResponse(any(),any(),any(),any(),any());
    }

    @Test
    public void trainAvailability_getAvailabilityForYesterday(){

        //Arrange
        Train train1 = Mockito.mock(Train.class);
        Train train2 = Mockito.mock(Train.class);
        List<Train> trainList = Arrays.asList(train1, train2);

        AvailabilityResponse expectedAvailabilityResponse = getAvailabilityResponse();

        LocalDate yesterday = LocalDate.now().minusDays(1L);
        availabilityService.getAvailability( "originCity_code",
                "destinationCity_code", yesterday,
                1,2,3);

        Mockito.verify(trainService, times(0)).getAvailability(any(),any(),any());
        Mockito.verify(trainAvailabilityTransformer, times(0)).toTrainAvailabilityResponse(any(),any(),any(),any(),any());
    }

    //al menos 1 pasajero adulto cada 2 menores (child or infant)

    @Test
    public void trainAvailability_getAvailabilityFor_1Adult_0infant_0child(){

        //Arrange
        Train train1 = Mockito.mock(Train.class);
        Train train2 = Mockito.mock(Train.class);
        List<Train> trainList = Arrays.asList(train1, train2);

        AvailabilityResponse expectedAvailabilityResponse = getAvailabilityResponse();

        availabilityService.getAvailability( "originCity_code",
                "destinationCity_code", LocalDate.now().plusDays(1),
                1,2,3);

        Mockito.verify(trainService, times(1)).getAvailability(any(),any(),any());
        Mockito.verify(trainAvailabilityTransformer, times(1)).toTrainAvailabilityResponse(any(),any(),any(),any(),any());
    }

    @Test
    public void trainAvailability_getAvailabilityFor_1Adult_2infant_1child(){

        //Arrange
        Train train1 = Mockito.mock(Train.class);
        Train train2 = Mockito.mock(Train.class);
        List<Train> trainList = Arrays.asList(train1, train2);

        AvailabilityResponse expectedAvailabilityResponse = getAvailabilityResponse();

        availabilityService.getAvailability( "originCity_code",
                "destinationCity_code", LocalDate.now().plusDays(1),
                2,1,1);

        Mockito.verify(trainService, times(0)).getAvailability(any(),any(),any());
        Mockito.verify(trainAvailabilityTransformer, times(0)).toTrainAvailabilityResponse(any(),any(),any(),any(),any());
    }

    @Test
    public void trainAvailability_getAvailabilityFor_2Adult_2infant_1child(){

        //Arrange
        Train train1 = Mockito.mock(Train.class);
        Train train2 = Mockito.mock(Train.class);
        List<Train> trainList = Arrays.asList(train1, train2);

        AvailabilityResponse expectedAvailabilityResponse = getAvailabilityResponse();

        availabilityService.getAvailability( "originCity_code",
                "destinationCity_code", LocalDate.now().plusDays(1),
                1,2,2);

        Mockito.verify(trainService, times(1)).getAvailability(any(),any(),any());
        Mockito.verify(trainAvailabilityTransformer, times(1)).toTrainAvailabilityResponse(any(),any(),any(),any(),any());
    }

    @Test
    public void trainAvailability_getAvailabilityFor_1child(){

        //Arrange
        Train train1 = Mockito.mock(Train.class);
        Train train2 = Mockito.mock(Train.class);
        List<Train> trainList = Arrays.asList(train1, train2);

        AvailabilityResponse expectedAvailabilityResponse = getAvailabilityResponse();

        availabilityService.getAvailability( "originCity_code",
                "destinationCity_code", LocalDate.now().plusDays(1),
                0,1,0);

        Mockito.verify(trainService, times(0)).getAvailability(any(),any(),any());
        Mockito.verify(trainAvailabilityTransformer, times(0)).toTrainAvailabilityResponse(any(),any(),any(),any(),any());
    }

    @Test
    public void trainAvailability_getAvailabilityFor_10adults(){

        //Arrange
        Train train1 = Mockito.mock(Train.class);
        Train train2 = Mockito.mock(Train.class);
        List<Train> trainList = Arrays.asList(train1, train2);

        AvailabilityResponse expectedAvailabilityResponse = getAvailabilityResponse();

        availabilityService.getAvailability( "originCity_code",
                "destinationCity_code", LocalDate.now().plusDays(1),
                0,1,10);

        Mockito.verify(trainService, times(0)).getAvailability(any(),any(),any());
        Mockito.verify(trainAvailabilityTransformer, times(0)).toTrainAvailabilityResponse(any(),any(),any(),any(),any());
    }


    private List<Train> getTrains(){

        City originCity = new City("originCity_code", "originCity_name", "originCity_county");
        City destinationCity = new City("destinationCity_code", "destinationCity_name", "destinationCity_county");

        Station originStation = new Station("Station01", "OriginStation_name", "OriginStation_adress", originCity);
        Station destinationStation = new Station("Station02", "DestinationStation_name", "DestinationStation_adress", destinationCity);

        Departure departure = new Departure(
                LocalDate.of(2020,1,1),
                LocalDate.of(2021,12,31),
                Arrays.asList("10:10", "12:00"));

        Train train = new Train( "test001",  originStation,  destinationStation,  "TrainProvider");
        train.setDepartureList(Arrays.asList(departure));

        train.setPrice(new Price("USD", BigDecimal.valueOf(1), BigDecimal.valueOf(5), BigDecimal.valueOf(20)) );

        return Arrays.asList(train);
    }

    private AvailabilityResponse getAvailabilityResponse(){

        AvailabilityResponse availabilityResponse = new AvailabilityResponse();

        StationDto originStation = new StationDto();
        originStation.setName("Station01");
        originStation.setAdress("OriginStation_adress");

        StationDto destinationStation = new StationDto();
        destinationStation.setName("Station02");
        destinationStation.setAdress("DestinationStation_adress");

        TrainDto expectedTrain = new TrainDto();
        expectedTrain.setId("test001");
        expectedTrain.setOrigin(originStation);
        expectedTrain.setDestination(destinationStation);
        expectedTrain.setDepartureDate( "02-02-2020");
        expectedTrain.setDepartureTime("10:10, 12:00");
        expectedTrain.setProvider("TrainProvider");
        expectedTrain.setServiceType(ServiceTypeDto.ECONOMIC);
        expectedTrain.setPrice(new PriceDto("USD",BigDecimal.valueOf(71)));

        availabilityResponse.setTrainOptions(Arrays.asList(expectedTrain));
        return availabilityResponse;
    };

}