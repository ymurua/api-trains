package com.almundo.trains.dto.model;

import java.util.Objects;

public class StationDto {

    private String name;
    private String adress;

    public StationDto() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAdress() {
        return adress;
    }

    public void setAdress(String adress) {
        this.adress = adress;
    }

    @Override public boolean equals(Object o) {
        if ( this == o )
            return true;
        if ( o == null || getClass() != o.getClass() )
            return false;
        StationDto that = (StationDto) o;
        return Objects.equals(name, that.name) &&
                Objects.equals(adress, that.adress);
    }

    @Override public int hashCode() {
        return Objects.hash(name, adress);
    }

    @Override public String toString() {
        return "StationDto{" +
                "name='" + name + '\'' +
                ", adress='" + adress + '\'' +
                '}';
    }
}
