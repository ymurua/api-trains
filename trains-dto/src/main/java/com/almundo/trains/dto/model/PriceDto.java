package com.almundo.trains.dto.model;

import java.math.BigDecimal;
import java.util.Objects;

public class PriceDto {

    private String currency;
    private BigDecimal total;

    public PriceDto() {
    }

    public PriceDto(String currency, BigDecimal total) {
        this.currency = currency;
        this.total = total;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public BigDecimal getTotal() {
        return total;
    }

    public void setTotal(BigDecimal total) {
        this.total = total;
    }

    @Override public boolean equals(Object o) {
        if ( this == o )
            return true;
        if ( o == null || getClass() != o.getClass() )
            return false;
        PriceDto priceDto = (PriceDto) o;
        return Objects.equals(currency, priceDto.currency) &&
                total.compareTo(priceDto.total) == 0 ;
    }

    @Override public int hashCode() {
        return Objects.hash(currency, total);
    }

    @Override public String toString() {
        return "PriceDto{" +
                "currency='" + currency + '\'' +
                ", total=" + total +
                '}';
    }
}
