package com.almundo.trains.dto.model;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Objects;

public class TrainDto {

    private String id;
    private StationDto origin;
    private StationDto destination;
    private String DepartureDate;
    private String DepartureTime; //TODO: cambiar a LocalTime
    private LocalDateTime ArrivalDate;
    private LocalDateTime ArrivalTime;
    private String provider;
    private ServiceTypeDto serviceType;
    private PriceDto price;

    public TrainDto() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public StationDto getOrigin() {
        return origin;
    }

    public void setOrigin(StationDto origin) {
        this.origin = origin;
    }

    public StationDto getDestination() {
        return destination;
    }

    public void setDestination(StationDto destination) {
        this.destination = destination;
    }

    public String getDepartureDate() {
        return DepartureDate;
    }

    public void setDepartureDate(String departureDate) {
        DepartureDate = departureDate;
    }

    public String getDepartureTime() {
        return DepartureTime;
    }

    public void setDepartureTime(String departureTime) {
        DepartureTime = departureTime;
    }

    public LocalDateTime getArrivalDate() {
        return ArrivalDate;
    }

    public void setArrivalDate(LocalDateTime arrivalDate) {
        ArrivalDate = arrivalDate;
    }

    public LocalDateTime getArrivalTime() {
        return ArrivalTime;
    }

    public void setArrivalTime(LocalDateTime arrivalTime) {
        ArrivalTime = arrivalTime;
    }

    public String getProvider() {
        return provider;
    }

    public void setProvider(String provider) {
        this.provider = provider;
    }

    public ServiceTypeDto getServiceType() {
        return serviceType;
    }

    public void setServiceType(ServiceTypeDto serviceType) {
        this.serviceType = serviceType;
    }

    public PriceDto getPrice() {
        return price;
    }

    public void setPrice(PriceDto price) {
        this.price = price;
    }

    @Override public boolean equals(Object o) {
        if ( this == o )
            return true;
        if ( o == null || getClass() != o.getClass() )
            return false;
        TrainDto trainDto = (TrainDto) o;
        return Objects.equals(id, trainDto.id) &&
                Objects.equals(origin, trainDto.origin) &&
                Objects.equals(destination, trainDto.destination) &&
                Objects.equals(DepartureDate, trainDto.DepartureDate) &&
                Objects.equals(DepartureTime, trainDto.DepartureTime) &&
                Objects.equals(ArrivalDate, trainDto.ArrivalDate) &&
                Objects.equals(ArrivalTime, trainDto.ArrivalTime) &&
                Objects.equals(provider, trainDto.provider) &&
                Objects.equals(serviceType, trainDto.serviceType) &&
                Objects.equals(price, trainDto.price); //TODO: cambiar al método compare de bigdecimal
    }

    @Override public int hashCode() {
        return Objects.hash(id, origin, destination, DepartureDate, DepartureTime, ArrivalDate, ArrivalTime, provider,
                serviceType, price);
    }

    @Override public String toString() {
        return "TrainDto{" +
                "id='" + id + '\'' +
                ", origin=" + origin +
                ", destination=" + destination +
                ", DepartureDate=" + DepartureDate +
                ", DepartureTime=" + DepartureTime +
                ", ArrivalDate=" + ArrivalDate +
                ", ArrivalTime=" + ArrivalTime +
                ", provider='" + provider + '\'' +
                ", serviceType=" + serviceType +
                ", price=" + price +
                '}';
    }
}
