package com.almundo.trains.dto.response;

import com.almundo.trains.dto.model.TrainDto;

import java.util.List;
import java.util.Objects;

public class AvailabilityResponse {

    private List<TrainDto> trainOptions;

    public AvailabilityResponse() {
    }

    public List<TrainDto> getTrainOptions() {
        return trainOptions;
    }

    public void setTrainOptions(List<TrainDto> trainOptions) {
        this.trainOptions = trainOptions;
    }

    @Override public boolean equals(Object o) {
        if ( this == o )
            return true;
        if ( o == null || getClass() != o.getClass() )
            return false;
        AvailabilityResponse that = (AvailabilityResponse) o;
        return Objects.equals(trainOptions, that.trainOptions);
    }

    @Override public int hashCode() {
        return Objects.hash(trainOptions);
    }

    @Override public String toString() {
        return "AvailabilityResponse{" +
                "trainOptions=" + trainOptions +
                '}';
    }
}