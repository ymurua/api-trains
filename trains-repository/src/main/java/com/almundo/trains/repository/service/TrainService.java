package com.almundo.trains.repository.service;

import com.almundo.trains.model.Departure;
import com.almundo.trains.model.Price;
import com.almundo.trains.model.Station;
import com.almundo.trains.model.Train;
import com.almundo.trains.repository.TrainRepository;
import com.almundo.trains.repository.exceptions.EntityNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

@Service
@Component
public class TrainService {

    @Autowired
    private TrainRepository trainRepository;
    @Autowired
    private StationService stationService;

    public TrainService() { }

    public void saveTrain(Train train){
        trainRepository.save(train);
    }

    public List<Train> getAllTrains() {
        return trainRepository.findAll();
    }

    public Train getTrainbyCode(String trainId) {
        return trainRepository.findTrainById(trainId);
    }

    public Train createTrain(String code, String originStation, String destinationStation,
            String provider){
//Para crear un servicio de tren deben existir las estaciones de origen y de destino
            if(stationService.stationCodeExists(originStation) &&
                    stationService.stationCodeExists(destinationStation)){

                Station origin = stationService.getStationBycode(originStation);
                Station destination = stationService.getStationBycode(destinationStation);

                Train train = new Train(code, origin, destination, provider);

                trainRepository.save(train);

                return train;
            }
            throw  new EntityNotFoundException("Origin or destination station ");
    }

    public Train updateDepartureDates(String code, LocalDate inicialDate,
            LocalDate finalDate, List<String> departureTimes) {
    //El tren tiene que existir para poder agregarle un día y horario de salida
        if( Objects.nonNull(getTrainbyCode(code))){
            Train train = trainRepository.findTrainById(code);
            Departure departure = new Departure(inicialDate, finalDate, departureTimes);
            train.setDepartureList(Collections.singletonList(departure));

            trainRepository.save(train);
            return train;
        }

        return new Train(); //TODO: sacar esto y reemplazar por una excepción-
    }

    public Train updatePrice(String code, String currency, BigDecimal infant, BigDecimal child, BigDecimal adult) {

        if( Objects.nonNull(getTrainbyCode(code))){
            Train train = trainRepository.findTrainById(code);
            Price price = new Price(currency, infant, child, adult);
            train.setPrice(price);

            trainRepository.save(train);
            return train;
        }

        return new Train(); //TODO: sacar esto y reemplazar por una excepción-
    }

    public Train getAvailabilityByOriginAndDestination(String origin, String destination, LocalDate date){
        return trainRepository.findTrainByOriginAndDestinationCityCode(origin, destination);
    }

    public Train getAvailabilityByDate(LocalDate date){
        return trainRepository.findTrainByDate(date, date);
    }

    public List<Train> getAvailability(String origin, String destination, LocalDate date){
        return trainRepository.getTrainsAvailability(origin, destination, date, date);
    }
}
