package com.almundo.trains.repository;

import com.almundo.trains.model.City;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface CityRepository extends MongoRepository<City, String> {

    City findCityByCode(String code);

}
