package com.almundo.trains.repository.service;

import com.almundo.trains.model.City;
import com.almundo.trains.model.Station;
import com.almundo.trains.repository.StationRepository;
import com.almundo.trains.repository.exceptions.EntityAlreadyExistsException;
import com.almundo.trains.repository.exceptions.EntityNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;

@Service
public class StationService {

    @Autowired
    private StationRepository stationRepository;
    @Autowired
    private CityService cityService;

    public StationService() {
    }

    public Station createStation(String stationCode, String name, String cityCode, String address)
            throws EntityAlreadyExistsException, EntityNotFoundException {

        City city = cityService.getCity(cityCode);

        if ( Objects.nonNull(city) ) {
            if ( !stationExists(stationCode) ) {
                Station station = new Station(stationCode, name, address, city);
                stationRepository.save(station);

                return station;
            }else{
                throw new EntityAlreadyExistsException("station with code "+ stationCode + " ");
            }
        }
        throw new EntityNotFoundException("city with code "+cityCode+" ");
    }

    public Station getStation(String code) throws EntityNotFoundException {
        Station station = getStationBycode(code);
        if (Objects.isNull(station)){
            throw new EntityNotFoundException("station with code "+ code + " ");
        }
        return station;
    }


    private boolean stationExists(String code) {
        return Objects.nonNull(getStationBycode(code));
    }

    public boolean stationCodeExists(String code) throws EntityNotFoundException {
        return Objects.nonNull(getStationBycode(code));
    }

    public Station getStationBycode(String code) {
        return stationRepository.findStationByCode(code);
    }

    public List<Station> getAllStations() {
        return stationRepository.findAll();
    }

    public Station getStationByCity(City city) {
        return stationRepository.findStationByCity(city);
    }


    public Boolean existStationForCity(String cityCode) {

        try {
            City city = cityService.getCity(cityCode);
            return Objects.nonNull(getStationByCity(city));
        }catch (Exception e){
            throw new EntityNotFoundException("City "+ cityCode);
        }

    }
}
