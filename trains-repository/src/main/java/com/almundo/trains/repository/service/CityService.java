package com.almundo.trains.repository.service;

import com.almundo.trains.model.City;
import com.almundo.trains.repository.CityRepository;
import com.almundo.trains.repository.exceptions.EntityAlreadyExistsException;
import com.almundo.trains.repository.exceptions.EntityNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;

@Service
public class CityService {

    @Autowired
    private CityRepository cityRepository;

    public CityService() { }


    public City createCity(String code, String name, String country) throws EntityAlreadyExistsException {

        if ( Objects.isNull(cityRepository.findCityByCode(code))) {
            City city = new City(code, name, country);
            cityRepository.save(city);

            return city;
        }else{
            throw new EntityAlreadyExistsException("city with code "+ code + " ");
        }
    }

    public City getCity(String code) throws EntityNotFoundException {
         City city= getCityByCode(code);
         if(Objects.nonNull(city)){
             return city;
         }else{
             throw new EntityNotFoundException("city with code "+ code + " ");
         }
    }

    public List<City> getAllCities (){
        return cityRepository.findAll();
    }

    public Boolean cityExists(String code) {
        return Objects.nonNull(getCityByCode(code));
    }

    private City getCityByCode(String code) {
        return cityRepository.findCityByCode(code);
    }
}
