package com.almundo.trains.repository.exceptions;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.util.WebUtils;

import java.util.Collections;
import java.util.List;

@ControllerAdvice
public class GlobalExceptionHandler {

    @ExceptionHandler({
            EntityNotFoundException.class,
            EntityAlreadyExistsException.class
    })

    public final ResponseEntity<ApiError> handleException(Exception exception, WebRequest request) {
        HttpHeaders headers = new HttpHeaders();

        if (exception instanceof EntityNotFoundException) {
            HttpStatus status = HttpStatus.NOT_FOUND;
            EntityNotFoundException entityNotFoundException = (EntityNotFoundException) exception;

            return handleEntityNotFoundException(entityNotFoundException, headers, status, request);

        }
        if (exception instanceof EntityAlreadyExistsException) {
            HttpStatus status = HttpStatus.CONFLICT;
            EntityAlreadyExistsException entityAlreadyExists = (EntityAlreadyExistsException) exception;

            return handleEntityAlreadyExistsException(entityAlreadyExists, headers, status, request);

        } else { //TODO: si es otro tipo de excepción, devuelvo 500.
            HttpStatus status = HttpStatus.INTERNAL_SERVER_ERROR;
            return handleInternalException(exception, null, headers, status, request);
        }

}
    private ResponseEntity<ApiError> handleEntityNotFoundException(EntityNotFoundException exception,
            HttpHeaders headers, HttpStatus status,
            WebRequest request) {
        List<String> errors = Collections.singletonList(exception.getMessage());
        return handleInternalException(exception, new ApiError(errors), headers, status, request);
    }

    private ResponseEntity<ApiError> handleEntityAlreadyExistsException(EntityAlreadyExistsException exception,
            HttpHeaders headers, HttpStatus status,
            WebRequest request) {
        List<String> errors = Collections.singletonList(exception.getMessage());
        return handleInternalException(exception, new ApiError(errors), headers, status, request);
    }

    private ResponseEntity<ApiError> handleInternalException(Exception exception, ApiError body,
            HttpHeaders headers, HttpStatus status,
            WebRequest request) {
        if (HttpStatus.INTERNAL_SERVER_ERROR.equals(status)) {
            request.setAttribute(WebUtils.ERROR_EXCEPTION_ATTRIBUTE, exception, WebRequest.SCOPE_REQUEST);
        }
        return new ResponseEntity<>(body, headers, status);
    }
}
