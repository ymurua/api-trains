package com.almundo.trains.repository.exceptions;

public class EntityNotFoundException extends RuntimeException {

    private String entityName; //city, station, train

    public static EntityNotFoundException createWith(String entityName) {
        return new EntityNotFoundException(entityName);
    }

    public EntityNotFoundException(String entityName) {
        this.entityName = entityName;
    }

    @Override
    public String getMessage() {
        return  entityName + " not found";
    }
}