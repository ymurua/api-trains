package com.almundo.trains.repository.exceptions;

public class EntityAlreadyExistsException extends RuntimeException {

    private String entityName; //city, station, train

    public static EntityAlreadyExistsException createWith(String entityName) {
        return new EntityAlreadyExistsException(entityName);
    }

    public EntityAlreadyExistsException(String entityName) {
        this.entityName = entityName;
    }

    @Override
    public String getMessage() {
        return  entityName + " already exists";
    }
}