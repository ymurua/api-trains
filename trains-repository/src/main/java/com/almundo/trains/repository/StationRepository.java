package com.almundo.trains.repository;

import com.almundo.trains.model.City;
import com.almundo.trains.model.Station;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface StationRepository extends MongoRepository<Station, String> {

    Station findStationByCode(String code);

    Station findStationByName(String name);

    Station findStationByCity(City city);

}
