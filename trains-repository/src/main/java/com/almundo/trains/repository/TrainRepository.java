package com.almundo.trains.repository;

import com.almundo.trains.model.Train;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.List;

@Repository
public interface TrainRepository extends MongoRepository <Train, String> {

    Train findTrainById(String trainId);

    @Query(value = "{'origin.city.code': ?0, 'destination.city.code' :?1}")
    Train findTrainByOriginAndDestinationCityCode(String origin, String destination);

    @Query("{'departuresList.inicialDepartureDate' :{'$lte':?0}, 'departuresList.finalDepartureDate' :{'$gte':?1}}")
    Train findTrainByDate(LocalDate StartDate, LocalDate endDate);

    @Query("{'origin.city._id': ?0, 'destination.city._id' :?1, "
            + "'departureList.startDate' :{'$lte':?2},"
            + " 'departureList.endDate' :{'$gte':?3}}")
    List<Train> getTrainsAvailability(String origin, String destination, LocalDate startDate, LocalDate endDate);

}
