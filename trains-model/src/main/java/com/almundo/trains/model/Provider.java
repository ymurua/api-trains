package com.almundo.trains.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Objects;

@Document
public class Provider {

    @Id
    private String id;
    private String code;
    private String name;

    public Provider(String id, String code, String name) {
        this.id = id;
        this.code = code;
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override public boolean equals(Object o) {
        if ( this == o )
            return true;
        if ( o == null || getClass() != o.getClass() )
            return false;
        Provider provider = (Provider) o;
        return Objects.equals(id, provider.id) &&
                Objects.equals(code, provider.code) &&
                Objects.equals(name, provider.name);
    }

    @Override public int hashCode() {
        return Objects.hash(id, code, name);
    }

    @Override public String toString() {
        return "Provider{" +
                "id='" + id + '\'' +
                ", code='" + code + '\'' +
                ", name='" + name + '\'' +
                '}';
    }
}

