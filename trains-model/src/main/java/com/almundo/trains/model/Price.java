package com.almundo.trains.model;

import java.math.BigDecimal;
import java.util.Objects;

public class Price {
//TODO: pensar si acá pongo el serviceType
    private String currency;
    private BigDecimal infant;
    private BigDecimal child;
    private BigDecimal adult;

    //TODO: poner como obligatorio child y adult. si no existen las demas denominaciones pasar todos a adult
    public Price(String currency, BigDecimal infant, BigDecimal child, BigDecimal adult) {
        this.currency = currency;
        this.infant = infant;
        this.child = child;
        this.adult = adult;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public BigDecimal getInfant() {
        return infant;
    }

    public void setInfant(BigDecimal infant) {
        this.infant = infant;
    }

    public BigDecimal getChild() {
        return child;
    }

    public void setChild(BigDecimal child) {
        this.child = child;
    }

    public BigDecimal getAdult() {
        return adult;
    }

    public void setAdult(BigDecimal adult) {
        this.adult = adult;
    }

//    @Override public boolean equals(Object o) {
//        if ( this == o )
//            return true;
//        if ( o == null || getClass() != o.getClass() )
//            return false;
//        Price that = (Price) o;
//        return Objects.equals(currency, that.currency) &&
//                ObjectUtils.compare(child, that.child)== 0 &&
//                ObjectUtils.compare(youngPerson, that.youngPerson) == 0 &&
//                ObjectUtils.compare(adult, that.adult) == 0 &&
//                ObjectUtils.compare(elderly, elderly) == 0;
//    }

    @Override public String toString() {
        return "Price{" +
                "currency='" + currency + '\'' +
                ", infant=" + infant +
                ", child=" + child +
                ", adult=" + adult +
                '}';
    }
}
