package com.almundo.trains.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Objects;

@Document(collection = "Station")
public class Station {

    @Id
    private String code;
    private String name;
    private String adress;
    private City city;


    public Station(String code, String name, String adress, City city) {
        this.code = code;
        this.name = name;
        this.adress = adress;
        this.city = city;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAdress() {
        return adress;
    }

    public void setAdress(String adress) {
        this.adress = adress;
    }

    public City getCity() {
        return city;
    }

    public void setCity(City city) {
        this.city = city;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    @Override public boolean equals(Object o) {
        if ( this == o )
            return true;
        if ( o == null || getClass() != o.getClass() )
            return false;
        Station station = (Station) o;
        return Objects.equals(name, station.name) &&
         Objects.equals(city, station.city) &&
         Objects.equals(code, station.code) &&
                Objects.equals(adress, station.adress);
    }

    @Override public int hashCode() {
        return Objects.hash(code,name,city, adress);
    }

    @Override public String toString() {
        return "Station{" +
                "code='" + code + '\'' +
                "name='" + name + '\'' +
                "city='" + city + '\'' +
                ", adress='" + adress + '\'' +
                '}';
    }
}
