package com.almundo.trains.model;

import java.time.LocalDate;
import java.util.List;
import java.util.Objects;

public class Departure {

    private LocalDate startDate;
    private LocalDate endDate;
    private List<String> departureTimes;

    public Departure() {
    }

    public Departure(LocalDate startDate, LocalDate endDate,
            List<String> departureTimes) {
        this.startDate = startDate;
        this.endDate = endDate;
        this.departureTimes = departureTimes;
    }

    public LocalDate getStartDate() {
        return startDate;
    }

    public void setStartDate(LocalDate startDate) {
        this.startDate = startDate;
    }

    public LocalDate getEndDate() {
        return endDate;
    }

    public void setEndDate(LocalDate endDate) {
        this.endDate = endDate;
    }

    public List<String> getDepartureTimes() {
        return departureTimes;
    }

    public void setDepartureTimes(List<String> departureTimes) {
        this.departureTimes = departureTimes;
    }

    @Override public boolean equals(Object o) {
        if ( this == o )
            return true;
        if ( o == null || getClass() != o.getClass() )
            return false;
        Departure that = (Departure) o;
        return Objects.equals(startDate, that.startDate) &&
                Objects.equals(endDate, that.endDate) &&
                Objects.equals(departureTimes, that.departureTimes);
    }

    @Override public int hashCode() {
        return Objects.hash(startDate, endDate, departureTimes);
    }

    @Override public String toString() {
        return "Departure{" +
                "startDate=" + startDate +
                ", endDate=" + endDate +
                ", departureTimes=" + departureTimes +
                '}';
    }
}
