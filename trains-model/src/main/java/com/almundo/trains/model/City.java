package com.almundo.trains.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Objects;

@Document (collection = "cities")
public class City {

    @Id
    private String code;
    private String name;
    private String country;

    public City(String code, String name, String country) {
        this.code = code;
        this.name = name;
        this.country = country;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    @Override public boolean equals(Object o) {
        if ( this == o )
            return true;
        if ( o == null || getClass() != o.getClass() )
            return false;
        City city = (City) o;
        return Objects.equals(code, city.code) &&
            Objects.equals(country, city.country) &&
                Objects.equals(name, city.name);
    }

    @Override public int hashCode() {
        return Objects.hash(code, name);
    }

    @Override public String toString() {
        return "City{" +
                ", code='" + code + '\'' +
                ", name='" + name + '\'' +
                ", country='" + country + '\'' +
                '}';
    }
}
