package com.almundo.trains.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.LocalTime;
import java.util.List;

@Document(collection = "Train")
public class Train{
//TODO: pensar si conviene tener una lista de precios
//TODO: y asignar un tipo de servicio a cada precio
    @Id
   private String id;
   private Station origin;
   private Station destination;
   private String provider;
   private ServiceType serviceType;
   private Price price;
   private List<Departure> departureList;
   private LocalTime duration;

    public Train() {
    }

    public Train(String id, Station origin, Station destination, String provider) {
        this.id = id ;
        this.origin = origin;
        this.destination = destination;
        this.provider = provider;
        this.serviceType = ServiceType.ECONOMIC;
    }

    public Station getOrigin() {
        return origin;
    }

    public void setOrigin(Station origin) {
        this.origin = origin;
    }

    public Station getDestination() {
        return destination;
    }

    public void setDestination(Station destination) {
        this.destination = destination;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getProvider() {
        return provider;
    }

    public void setProvider(String provider) {
        this.provider = provider;
    }

    public ServiceType getServiceType() {
        return serviceType;
    }

    public void setServiceType(ServiceType serviceType) {
        this.serviceType = serviceType;
    }

    public Price getPrice() {
        return price;
    }

    public void setPrice(Price price) {
        this.price = price;
    }

    public List<Departure> getDepartureList() {
        return departureList;
    }

    public void setDepartureList(List<Departure> departureList) {
        this.departureList = departureList;
    }


}